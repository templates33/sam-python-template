create-requirements:
	@pipenv lock -r > layers/requirements.txt

build: create-requirements
	@sam build $(target)
