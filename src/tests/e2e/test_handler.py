"""
e2e test example using an http event V2
"""
import json

import pytest

from src.handlers.placeholder import lambda_handler


@pytest.fixture()
def event():
    with open('src/tests/events/http-event-2.json') as fh:
        return json.loads(fh.read())


def test_lambda_handler(event):
    ret = lambda_handler(event, "")
    data = json.loads(ret["body"])
    message = json.loads(data["message"])
    assert ret["statusCode"] == 200
    assert message["rawPath"] == "/Echo"
